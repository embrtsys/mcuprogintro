/**
 * @filename
 * progintro.c
 * 
 *  @brief
 *  Introductory exercises to micro-controller programming
 * 
 *  These are the exercises for MCU programming. 
 * 
 * Exercise: Simple I/O
 * 
 * 3. When the control input changes from '0' to '1', the 8 bit port outputs 
 * the sequence 'ABABAB', where 'A' and 'B' are any two different values. 
 * When the sequence ends, the output should remain with the value 'B'. 
 * 
 * Note 1: RC2 (Logical Pin 22 in Max32 Board) will be the control pin. 
 * 
 * Note 2: code has been refactored for structure.
 */

#include "../Common/ConfigBits/config_bits.h"

#include <xc.h>
#include <stdint.h>
#include <stdbool.h>

#define VALUE_A     0x55
#define VALUE_B     0xAA

#define PUSHBUTTON  PORTCbits.RC2

void init(void);
void delay(void);

int main(void) {

    uint8_t pbPrevState; /*! Push-button previous state */
    uint8_t count;

    init();

    pbPrevState = PUSHBUTTON;

    while (1) {

        if (PUSHBUTTON != pbPrevState) {
            /* Push-button has changed */
            pbPrevState = PUSHBUTTON; /* store current state */
            if (PUSHBUTTON == 1) 
            {
                /* Push button has been released */
                /* Execute action: */
                for (count = 0; count < 3; count++) 
                {
                    PORTB == VALUE_A;
                    /* Toggle LED4/RA3 to signal*/
                    PORTAINV = 0x08;
                    delay();
                    PORTB = VALUE_B;
                    /* Toggle LED4/RA3 to signal*/
                    PORTAINV = 0x08;
                    delay();
                }
            }

        }
    }
}


void init(void)
{
    /* TRIS registers config: */

    /* Use PORT B as output 
     * This port is available as an 8 bit port in J5 connector in Max32 */
    TRISB = 0x00; /* all outputs */

    /* Configure RA3 as Output */
    TRISAbits.TRISA3 = 0;

    /* RC2 is input */
    TRISCbits.TRISC2 = 1; /* Set RC2 as an input */


    /* Set initial value for PORTB (outputs) */
    PORTB = VALUE_A;
    
    /* Set LD4 initially ON */
    PORTAbits.RA3 = 1;
}

void delay(void) {
    uint32_t counter;
    const uint32_t MaxCount = 400000;

    /* Delay... */
    for (counter = 0; counter < MaxCount; counter++) {
        asm("nop");
    }
}